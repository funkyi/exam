const http = require('http')
const requestify = require('requestify');
const HtmlTableToJson = require('html-table-to-json');

const fundCode = process.argv[2]

if (fundCode == null) { console.log('Require 1 arg fund code') }
var data = JSON.stringify({ 'important': 'data' });
const cookieString = 'hasCookie=true'
const url = 'https://codequiz.azurewebsites.net'
const fundsName = [
    'B-INCOMESSF', 'BM70SSF', 'BEQSSF', 'B-FUTURESSF'
]
var isFound = false;
requestify.request(url, { method: 'GET', cookies: { hasCookie: 'true' }, dataType: 'json' })
    .then(function (response) {
        response.getBody();
        response.getHeaders();
        response.getHeader('Accept');
        response.getCode();

        // Get the response raw body
        data = response.body;

        const jsonTables = HtmlTableToJson.parse(data);
        const json = jsonTables.results[0];

        json.forEach((f) => {
            if (fundCode == f['Fund Name']) {
                isFound = !isFound;
                console.log(`Fund:: ${fundCode} :: Nav value (${f.Nav})`)
            }
        })

        if (!isFound) {
            console.log('Fund not found!!!')
        }

    });

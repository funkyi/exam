import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categororyListFilter',
})
export class CategororyListFilterPipe implements PipeTransform {
  transform(categories: any[], checkArgs: string) {
    if (!checkArgs) {
      return categories;
    }

    const valueRegex = new RegExp(checkArgs, 'gi');
    return categories.filter((category) => {
      return valueRegex.test(category.licenseNumber);
    });
  }
}

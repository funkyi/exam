import { Component } from '@angular/core';
import axios from 'axios'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Filter';
  private categoriesList = [];
  filters = [];
  searchText = '';
  items = [
    {
      name: 'item',
      id: 1
    },
    {
      name: 'item',
      id: 2
    },
    {
      name: 'item',
      id: 3
    },
  ]

  ngOnInit() {
    console.log('Init')
    this.getCategoriesList();
  }

  async fetchData() {
    const resp = await fetch('https://api.publicapis.org/categories');
    const data = await resp.json();
    return data;
  }

  private async getCategoriesList() {
    this.categoriesList = await this.fetchData();
    this.filters = this.categoriesList;
  }

  getSearchResult(searchValue: string) {
    if (searchValue.length <= 0) {
      this.filters = this.categoriesList;
    } else {
      this.filters = this.categoriesList.filter(category => {
        const valueRegex = new RegExp(searchValue, 'gi');
        return valueRegex.test(category);
      });
    }

  }
}
